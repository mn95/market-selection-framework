This was produced a part of a Global Health Capstone Project at Duke University.
Work of Morgan Daly, Mckenzie Hollen, Noelani Ho, Manish Nair, Zarah Udwadia
Please direct any queries regarding program to Manish Nair.

The files in this project allow for the sorting of countries based on their
candidacy for women's health technology scale-up. Add the required data to
the excel file, and the program will produce results on an R-Shiny platform.

Note: It will be necessary to adjust the global.R file to reflect your working directory during the data reading.
